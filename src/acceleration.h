#ifndef __ACCEL_H__
#define __ACCEL_H__

#include <stdint.h>
#include "iib.h"

uint16_t get_accel_curve(IIB *iib, int i);

#endif
