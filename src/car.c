#include "car.h"
#include "can-ids/CAN_IDs.h"
#include "io.h"
#include "lib_pic33e/flash.h"
#include "safety.h"

/// Extern variables
extern uint32_t wtd_arm;
extern uint32_t wtd_revive_arm;
extern uint32_t wtd_torque_encoder;

extern uint16_t te_alive_timer;

/// Global
uint16_t te_alive;

CANdata msg_rtd;
CANdata aux_rtd_msg;
COMMON_MSG_RTD_ON message_ON;
ARM_CAN_data ARM_message;
TE_CAN_Data TE_message;
MASTER_MSG_Data master_message;


void handle_set_parameter(CANdata msg, IIB *iib) {
	uint16_t parameter = msg.data[1];

	if (parameter >= IIB_PARAMETER_NUMBER) {
		return;
	}

	uint16_t value = msg.data[2];

	if (value < iib->sets[parameter].min || value > iib->sets[parameter].max) {
		return;
	}

	*(iib->sets[parameter].p) = msg.data[2];

	//write_flash(msg.data+2, parameter, 1);

	msg.dev_id  = DEVICE_ID_IIB;
	send_can2(msg);

	return;
}

void handle_get_parameter(CANdata msg, IIB *iib) {
	uint16_t parameter = msg.data[1];
	if (parameter >= IIB_PARAMETER_NUMBER) {
		return;
	}

	uint16_t value = *(iib->sets[parameter].p);

	msg.dev_id = DEVICE_ID_IIB;
	msg.dlc = 6;
	msg.data[2] = value;
	send_can2(msg);

	return;
}

/**
*	Name: handle_set_msg;
*	Description: If we receive a set message, verify wich parameter is to be changed, and run the handle process
*	Input's: set message data, IIB structure
*	Output: void
*/
void handle_set_msg(CANdata msg, IIB *iib){

	uint16_t parameter;

	handle_set_parameter(msg, iib);

	return;
}

/**
*	Name: handle_get_msg;
*	Description: If we receive a get message, verify wich parameter is to be read, and run the handle process
*	Input's: set message data, IIB structure
*	Output: void
*/
void handle_get_msg(CANdata msg, IIB *iib){

	uint16_t parameter;

	handle_get_parameter(msg, iib);
	return;
}


/**
*	Name: turn_on_sequence;
*	Description: Executes the inverter turn on sequence
*	Input's: IIB structure
*	Output: void
*/
void turn_on_sequence(IIB *iib){

	/// CAR is now RTD
	iib->car.status.car_rtd = 1;
	/// We are in turning on process
	iib->turning_on = 1;

	set_turn_on_all(iib);	/** Sets controll word for turn on */
	set_idle_all(iib);		/** need to set setpoints to 0 */

	msg_rtd = make_rtd_on_msg(DEVICE_ID_IIB, RTD_STEP_INV_GO);	/** Sends acknoledge message for dash led */
 	send_can2(msg_rtd);

	return;
 }


/**
*	Name: turn_off_sequence;
*	Description: Executes the inverter turn off sequence
*	Input's: IIB structure, what triggered the turn off sequence (mode)
*	Output: void
*/
void turn_off_sequence(int mode, IIB *iib){

	set_turn_off_all(iib);	/** Sets controll word */

	/// CAR is no longer in RTD
	iib->car.status.car_rtd = 0;
	/// We are in turning off process
	iib->turning_off = 1;

	/// Sees what made us turnoff and send the acknoledge message
	switch(mode){

	 	case 1 : //We decided to turn off

	 		msg_rtd = make_rtd_off_msg(DEVICE_ID_IIB, 2);
	 		msg_rtd.dlc = 4;
	 		msg_rtd.data[1] = mode;

	 		send_can2(msg_rtd);

	 		return;

	 	case 2 : //We got a turn off request

	 		msg_rtd = make_rtd_off_msg(DEVICE_ID_IIB, 1);
	 		msg_rtd.dlc = 4;
	 		msg_rtd.data[1] = mode;

	 		send_can2(msg_rtd);

	 		return;
 	}

	return;
}


/**
*	Name: handle_rtd_on_message;
*	Description: handles an RTD on message, sees number of step and if iib is ok
*	Input's: IIB structure, msg data
*	Output: void
*/
void handle_rtd_on_message(IIB *iib, CANdata msg){

	parse_can_common_RTD(msg, &message_ON);

	/// TE ok step and iib is ready continue the RTD sequence
	if(message_ON.step == RTD_STEP_TE_OK){

		/// Inverters ok
 		if(iib->inv_rtd == 1){

 			aux_rtd_msg = make_rtd_on_msg(DEVICE_ID_IIB, RTD_STEP_INV_OK);
 			send_can2(aux_rtd_msg);
 		}

  		/// Inverters not ok
  		else if(iib->inv_rtd == 0){

			aux_rtd_msg = make_rtd_on_msg(DEVICE_ID_IIB, RTD_STEP_INV_NOK);
  			send_can2(aux_rtd_msg);
 		}
 	}

	/// Te is not ok
	if(message_ON.step == RTD_STEP_TE_NOK){

	 	iib->car.status.car_rtd = 0;	/** CAR is not in rtd condition */

	 	/// If the inverters are on, turn off
	 	if(iib->inv_on == 1){

	 		turn_off_sequence(TURN_OFF_REQUEST, iib);
	 	}
	}

	/// DCU step
	else if(message_ON.step == RTD_STEP_DCU){

		/// Inverters already on
		if(iib->inv_on == 1){
			return;
		}

		/// Inverters ok
		if(iib->inv_rtd == 1){

			/// Lets jardate motherfuckeeeer!!!(by: Pedro Santos)
			turn_on_sequence(iib);
		}

  	 	/// Inverters not ok
 	 	else if(iib->inv_rtd == 0){

 	 		aux_rtd_msg = make_rtd_on_msg(DEVICE_ID_IIB, RTD_STEP_INV_NOK);
	  		send_can2(aux_rtd_msg);
  	 	}
	}

 	return;
}


/**
*	Name: process_car_message;
*	Description: Processes which device sended a message, and handle it
*	Input's: IIB structure, msg data
*	Output: void
*/
void process_car_message(IIB *iib, CANdata msg){


	/// SET MESSAGE
	if(msg.msg_id == CMD_ID_COMMON_SET && msg.data[0] == DEVICE_ID_IIB){
		LED2_WRITE ^= 1;
	 	handle_set_msg(msg, iib);	/** process the set message */
	 	return;
	}

	/// GET MESSAGE
	if(msg.msg_id == CMD_ID_COMMON_GET && msg.data[0] == DEVICE_ID_IIB){

		LED2_WRITE ^= 1;
	 	handle_get_msg(msg, iib);	/** process the get message */
	 	return;
	}

	/// RTD ON MESSAGE
	if (msg.msg_id ==  CMD_ID_COMMON_RTD_ON){

		handle_rtd_on_message(iib, msg);	/** process rtd on message */
		return;
	}

	/// RTD OFF MESSAGE
 	if(msg.msg_id == CMD_ID_COMMON_RTD_OFF){

 		/// Turn car off
 		/// Start turnoff sequence, acknoledge the turnoff request
 		turn_off_sequence(TURN_OFF_REQUEST, iib);
 		return;
 	}

	/// Verify wich device sended a message, and read the message
	switch(msg.dev_id){

		case DEVICE_ID_ARM 					: handle_ARM(msg, iib);  break;
		case DEVICE_ID_TE  					: handle_TE(msg, iib);   break;
		case DEVICE_ID_BMS_MASTER   		: handle_BMS(msg, iib);  break;
		//case DEVICE_ID_INTERFACE			: handle_INTERFACE(msg, iib); break;
	}

	return;
}

/**
*	Name: handle_ARM;
*	Description: handles ARM messages
*	Input's: IIB structure, msg data
*	Output: void
*/
void handle_ARM(CANdata msg, IIB *iib){

	int motor_num = 0, i = 0;

	if(!iib->arm_wtd_exceded){

		wtd_arm = 0;
		iib->car.status.arm_ok = 1;

	}else if(iib->arm_wtd_exceded){

		iib->arm_message_number++;

		if(wtd_revive_arm > 250){	///maior que 5 segundos

			if(iib->arm_message_number > 20){

				wtd_arm = 0;
				iib->car.status.arm_ok = 1;
				iib->arm_message_number = 0;
				iib->arm_wtd_exceded = 0;
				iib->input_mode = ARM_MODE;
			}
		}
	}

	parse_can_ARM(msg, &ARM_message);

	if(msg.msg_id == 9/*MSG_ID_ARM_SETPOINTS*/){

		if(iib->input_mode == ARM_MODE){

			motor_num = msg.data[0];//ARM_message.setpoint.motor_num;

			iib->inverter[motor_num].setpoints.target_setpoint 	= msg.data[1];//ARM_message.setpoint.setpoint;
		 	iib->inverter[motor_num].setpoints.torque_limit_positive = msg.data[2];//ARM_message.setpoint.torque_pos;
			iib->inverter[motor_num].setpoints.torque_limit_negative = msg.data[3];//ARM_message.setpoint.torque_neg;

			if(!iib->regen_on){

				iib->inverter[motor_num].setpoints.torque_limit_negative = 0;
			}
		}

		else if(iib->input_mode == TEST_BENCH_MODE){

			for(i = 0; i < 4 ; i++){

		 		iib->inverter[i].setpoints.target_setpoint = msg.data[1];//ARM_message.setpoint.setpoint;
		 		iib->inverter[i].setpoints.torque_limit_positive = msg.data[2];//ARM_message.setpoint.torque_pos;
		 		iib->inverter[i].setpoints.torque_limit_negative = 0;
			}
		}
	}

    return;
}


/**
*	Name: handle_TE;
*	Description: handles TE messages
*	Input's: IIB structure, msg data
*	Output: void
*/
void handle_TE(CANdata msg, IIB *iib){

	wtd_torque_encoder = 0;
	iib->car.status.te_ok = 1;
	//uint16_t brakes = 0;
	//int margin = 100;

	int i = 0;

	if(msg.msg_id == MSG_ID_TE_MAIN){

		parse_can_te(msg, &TE_message);

	 	if(iib->input_mode == DEFAULT_MODE){

	 		iib->car.te_percentage = TE_message.main_message.APPS;
	 		//brakes = TE_message.main_message.BPS_electric;

	 		// if(brakes > margin && iib->regen_on == 1){

	 		// 	for (i = 0; i < 4; i++){

	 		// 		iib->inverter[i].setpoints.target_speed = 0;
	 		// 		iib->inverter[i].setpoints.torque_limit_positive = 0;
	 		// 		iib->inverter[i].setpoints.torque_limit_negative = (int16_t)((-5000)*(brakes/10000.0));
	 		// 	}
	 		// }else{

	 		///if(iib->amk_control_mode == SPEED_CONTROL){

	 			for(i = 0; i < 4; i++){

			 		iib->inverter[i].setpoints.target_setpoint = iib->iib_safety.max_rpm;
			 		iib->inverter[i].setpoints.torque_limit_positive = (uint16_t)(iib->iib_safety.inverter_limits[i].max_operation_torque*(iib->car.te_percentage/10000.0));
			 		iib->inverter[i].setpoints.torque_limit_negative = 0; /// in default only positive torque is set
		 		}

	 		//}else if(iib->amk_control_mode == TORQUE_CONTROL){

	 			// for(i = 0; i <4; i++){

	 			// 	iib->inverter[i].setpoints.target_setpoint = (uint16_t)(iib->iib_safety.inverter_limits[i].max_operation_torque*(iib->car.te_percentage/10000.0));
	 			// 	iib->inverter[i].setpoints.torque_limit_positive = iib->iib_safety.inverter_limits[i].max_operation_torque;
	 			// 	iib->inverter[i].setpoints.torque_limit_negative = 0;
	 			// }
	 		//}
	 	}
	}



	//TE_CAN_Data TE_message;
	//uint16_t TE_status_aux = 0;

	//wtd_torque_encoder = 0;	/** Clear TE watchdog timer */

	// /// Check if TE is ok again
	// if(iib->input_mode == ERROR_MODE && iib->car.te_alive_again == 0){

	// 	/// If the te alive timer as passed a certaint time we continue to ignore
	// 	if(te_alive_timer >= 2){

	// 		te_alive = 0;
	// 	}

	// 	te_alive_timer = 0;	/** start timer to confirm that we receive another message in a period of 1200ms */

	// 	te_alive++;

	// 	if(te_alive >= 3){

	// 		/// Te is good again, go to default mode
	// 		iib->car.te_alive_again == 1;
	// 		iib->input_mode = DEFAULT_MODE;
	// 	}
	// }

	/// Test bench mode we dont care if te is ok, only see if is sending messages
	// if(iib->input_mode == TEST_BENCH_MODE){

	// 	iib->car.status.te_ok = 1;
	// }

	// /// If we are not in test bench, run
	// if(iib->input_mode != TEST_BENCH_MODE){

	// 	parse_can_te(msg, &TE_message);

	// 	switch(msg.msg_id){

	// 		case MSG_ID_TE_MAIN :	/** TE MAIN message */

	// 			/// Verify TE status
	// 			iib->car.te_percentage = TE_message.main_message.APPS;


	// 			/// METER AQUI UMA MENSAGEM COM A PERCENTAGEM DO PEDAL

	// 			TE_status_aux = TE_message.main_message.status.TE_status;

	// 			TE_status_aux = TE_status_aux && 0b00000000000000000;//0b00000101100001111;

	// 			if(TE_status_aux != 0){		/** status indicates an error */

	// 				iib->car.status.te_ok = 0;
	// 				set_idle(iib);

	// 			}else if(TE_status_aux == 0){	/** status is ok */

	// 				/// TE is good
	// 				iib->car.status.te_ok = 1;
	// 			}

	// 			/// TE controlls setpoints if we are in default mode, the car and inverters are in RTD, and TE is good
	// 				if(iib->input_mode == DEFAULT_MODE  && (iib->car.status.car_rtd && iib->inv_rtd) == 1
	// 				&& iib->car.status.te_ok == 1){

	// 				int i = 0;

	// 				/// updates values for all motors
	// 				for(i = 0; i < 4; i++){

	// 					iib->inverter[i].setpoints.target_speed = iib->iib_safety.max_rpm;
	// 					iib->inverter[i].setpoints.torque_limit_positive =
	// 					(int16_t)((/*iib->iib_safety.inverter_limits[i].max_operation_torque*/2000)*(iib->car.te_percentage/10000.0));

	// 					iib->inverter[i].setpoints.torque_limit_negative = 0; /// in default only positive torque is set
	// 				}
	// 			}

	// 			break;
	// 	}
	// }
	//
	return;
}


/**
*	Name: handle_BMS;
*	Description: handles BMS messages
*	Input's: IIB structure, msg data
*	Output: void
*/
void handle_BMS(CANdata msg, IIB *iib){

	/// Check master battery status
	if(msg.msg_id == MSG_ID_MASTER_STATUS){

		parse_can_message_master(msg, &master_message);

		/// If ther is high volt#ifage in the inverter housing and the AIR+ is opened, there is implausability
		if(/*iib->car.status.inv_on &&*/ !master_message.status.ts_on){

			turn_off_sequence(TURN_OFF_REQUEST, iib);	/** turnoff inverters */
		}
		iib->car.status.inv_hv = master_message.status.AIR_positive;	/** Update inverter high voltage status */
	}

	/// BMS voltage
	else if(msg.msg_id == MSG_ID_MASTER_ENERGY_METER){

		parse_can_message_master(msg, &master_message);

		iib->car.bms_voltage = (uint16_t)(master_message.energy_meter.voltage/100.0);	/* converts voltage units to [V] */
	}

	return;
}
