#ifndef __CAR1_H__
#define __CAR1_H__

#include "iib.h"

#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/DASH_CAN.h"
#include "can-ids/Devices/TE_CAN.h"
#include "can-ids/Devices/COMMON_CAN.h"
#include "can-ids/Devices/ARM_CAN.h"
#include "can-ids/Devices/INTERFACE_CAN.h"
#include "can-ids/Devices/DASH_STEER_CAN.h"
#include "can-ids/Devices/BMS_MASTER_CAN.h"
#include "can-ids/Devices/IIB_CAN.h"

/// Who asked for turn-off Requests
#define WE_TURN_OFF 	 2
#define TURN_OFF_REQUEST 1

void handle_set_parameter(CANdata msg, IIB *iib);
void handle_set_msg(CANdata msg, IIB *iib);
void turn_on_sequence(IIB *iib);
void turn_off_sequence(int mode, IIB *iib);
void handle_rtd_on_message(IIB *iib, CANdata msg);
void process_car_message(IIB *iib, CANdata msg);
void handle_ARM(CANdata msg, IIB *iib);
void handle_TE(CANdata msg,IIB *iib);
void handle_BMS(CANdata msg, IIB *iib);

#endif
