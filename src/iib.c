/*! \file
 *  \brief  IIB functions
 */

#include "iib.h"
#include "io.h"
#include <math.h>
#include "can-ids/Devices/IIB_CAN.h"

extern uint16_t line;

/// Extern variable
//extern uint16_t wtd_arm;


/**
*   Name: convert_torque_unit
*   Description: converts torque values to 0.1% of nominal torque, unit specified by datasheet to send
*   Input's: IIb structure containing the info about all 4 inverters torque setpoints
*   Output: void
*/
void convert_torque_unit(IIB *iib){

    int i = 0;
    int16_t torque_converted_n;  /** percentage of nominal torque 0.1%Mn */
    uint16_t torque_converted_p;

    for(i = 0; i < 4; i++){

        /** positive torque */
        torque_converted_p = iib->inverter[i].setpoints.torque_limit_positive / NOMINAL_TORQUE;
        iib->inverter[i].setpoints.torque_limit_positive = torque_converted_p;

        /** negative torque */
        torque_converted_n = ((int16_t)iib->inverter[i].setpoints.torque_limit_negative) / NOMINAL_TORQUE;
        iib->inverter[i].setpoints.torque_limit_negative = torque_converted_n;
    }

	return;
}


/**
*   Name: send_to_amk
*   Description: final stage before sending the setpoint message, prepares the 4 messages one for each motor to be sent
*   Input's: IIB structure containing 4 inverters, with all the id's and the substructure setpoints were the message 
*            parameters are found,
*   Output: void
*/
void send_to_amk(IIB *iib){

    int i = 0;
    CANdata inverters_ref[4];
    CANdata debug_iib[4];
    uint16_t original_positive_torque[4];
    int16_t original_negative_torque[4];

    /// Save original torque values so we can keep them later
    for(i = 0; i < 4; i++){

        original_positive_torque[i] = iib->inverter[i].setpoints.torque_limit_positive;
        original_negative_torque[i] = iib->inverter[i].setpoints.torque_limit_negative;
    }
    
    convert_torque_unit(iib);   /** Before sending, the torques are converted to the correct unit */

    /** References to inverters */
    for(i = 0; i < 4; i++){

        inverters_ref[i].sid = iib->inverter[i].reference_id;

        debug_iib[i].dev_id = 16;
        debug_iib[i].msg_id = i + 50;
        debug_iib[i].dlc = 8;

        inverters_ref[i].dlc = 8;
        
        inverters_ref[i].data[0] = iib->inverter[i].setpoints.control_word.word;
        /** Target speed for the motor */
        inverters_ref[i].data[1] = iib->inverter[i].setpoints.target_setpoint; 
        /** Positive Torque for motor */
        inverters_ref[i].data[2] = iib->inverter[i].setpoints.torque_limit_positive;
        
        /** Negative Torque for motor */
        inverters_ref[i].data[3] = iib->inverter[i].setpoints.torque_limit_negative;
        
        debug_iib[i].data[0] = iib->inverter[i].setpoints.control_word.word;
        debug_iib[i].data[1] = iib->inverter[i].setpoints.target_setpoint;    
        debug_iib[i].data[2] = iib->inverter[i].setpoints.torque_limit_positive;          
        debug_iib[i].data[3] = iib->inverter[i].setpoints.torque_limit_negative;
    }

    /// We if are about to turn on, see wich inverter's are active and enable the BE's
    /// Do this before sending to inverters


    for(i = 0; i < 4; i++){    /** Send only to the inverters that are active */
        
        if(iib->debug_mode){

            send_can2(debug_iib[i]);
        }

        if(iib->inverter[i].inverter_active == 1){
            send_can1(inverters_ref[i]);  /** Send to inverter can line */
        }
    }

    /// Turning OFF
    if(iib->turning_off == 1){

        iib->turning_off = 0;   /** Turn off process over */
        iib->inv_on = 0;        /** Inverters are now off */
    }

    /// Turning ON
    if(iib->turning_on == 1){

        //wtd_arm = 0;            /** Start counting ARM watchdog */
        iib->inv_on = 1;        /** Acknoledge that inverters are on */
        iib->turning_on = 0;    /** Stop turning on */
    }
    
    /// Return original torque setpoints
    for(i = 0; i < 4; i++){

        iib->inverter[i].setpoints.torque_limit_positive = original_positive_torque[i];
        iib->inverter[i].setpoints.torque_limit_negative = original_negative_torque[i];
    }

	return;
}


void set_idle(IIB *iib, int inverter){

    if (inverter != 0 && inverter != 1 && inverter != 2 && inverter != 3) {
        return;
    }

    iib->inverter[inverter].setpoints.torque_limit_positive = 0;
    iib->inverter[inverter].setpoints.torque_limit_negative = 0;
    iib->inverter[inverter].setpoints.target_setpoint = 0;

    return;
}

/**
*   Name: set_idle
*   Description: sets all setpoints to send to the motor's at 0
*   Input's: IIB structure containing the 4 inverters where we can update the setpoints for each inverter
*   Output: void
*/
void set_idle_all(IIB *iib){
    
    int i = 0;

    for(i = 0; i < 4; i++){

        set_idle(iib, i);
    }

	return;
}


void set_turn_off(IIB *iib, int inverter){

    if (inverter != 0 && inverter != 1 && inverter != 2 && inverter != 3) {
        return;
    }
    
    set_idle(iib, inverter);

    iib->inverter[inverter].setpoints.control_word.b_inverter_on = 0;
    iib->inverter[inverter].setpoints.control_word.b_dc_on = 1;
    iib->inverter[inverter].setpoints.control_word.b_enable = 0;
    iib->inverter[inverter].setpoints.control_word.b_error_reset = 0;

    return;
}

/**
*   Name: set_turn_off
*   Description: prepares for the turn off sequence
*   Input's: IIB structure containing the inverter setpoints and the respective control word
*   Output: void
*/
void set_turn_off_all(IIB *iib){
    
    int i = 0;

    for(i = 0; i < 4; i++){ 

        set_turn_off(iib, i);
    }

	return;
}


/**
*   Name: transform_id_to_inverter
*   Description: Based upon a msg id, decodes from what inverter it is
*   Input's: the CAN message containing the id, IIB structure to compare with all id's
*   Output: corresponding inverter number (1,2,3,4)
*/
int transform_id_to_inverter (IIB * iib, CANdata msg){

    int i = 0;

    for(i = 0; i < 4; i++){

        /** check if id is any of the 2 messages id's from a specific inverter */
        if(msg.sid == iib->inverter[i].message1_id || msg.sid == iib->inverter[i].message2_id){
            return i;
        }
    }   

    return -1;  /** message doesn't match any knowned id(NOT GOOD) */
}


/**
*   Name: calculate_torque
*   Description: Calculates individual motor torque's
*   Input's: IIB structure, inverter number
*   Output: void
*/ 
void calculate_torque(IIB *iib, int inverter){

    float iq = 0;    

    /// Se amk datasheet for this calculation
    iq = (iib->inverter[inverter].torque_current * CONVERTER_PEAK_CURRENT)/16384.0;

    /// Compute motor torque
    iib->inverter[inverter].torque = (int16_t)(iq*MOTOR_KT);

	return;
}


/**
*   Name: calculate_current
*   Description: Calculates individual motor current
*   Input's: IIB structure, inverter number
*   Output: void
*/ 
void calculate_current(IIB *iib, int inverter){

    uint16_t id, iq;

    /// Computes id and iq
    id = (iib->inverter[inverter].magnetizing_current * CONVERTER_PEAK_CURRENT)/16384;
        
    iq = (iib->inverter[inverter].torque_current * CONVERTER_PEAK_CURRENT)/16384;

    /// Module of vectorial sum
    iib->inverter[inverter].current = sqrt(id*id + iq*iq);  /** this is the current module in [A] */

	return;
}


/**
*   Name: update_inverter_actual_values_1
*   Description: Updates information that came from message 1
*   Input's: the number of the inverter we want to update the info, the information(data), IIB structure
*   Output: void
*/
void update_inverter_actual_values_1(IIB *iib, int inverter_num, uint16_t message_data[4]){
    
    if (inverter_num != 0 && inverter_num != 1 && inverter_num != 2 && inverter_num != 3) {
        return;
    }
    
    iib->inverter[inverter_num].status.word = message_data[0];          /// Status word
    iib->inverter[inverter_num].actual_speed = message_data[1];         /// Actual motor speed
    iib->inverter[inverter_num].torque_current = message_data[2];       /// Torque current
    iib->inverter[inverter_num].magnetizing_current = message_data[3];  /// Magnetizing current

    /// calculate torque and current for that inverter
    //calculate_torque(iib, inverter_num);
    //calculate_current(iib, inverter_num);
	//
	return;
}


/**
*   Name: update_inverter_actual_values_2
*   Description: Updates information that came from message 2
*   Input's: the number of the inverter we want to update the info, the information(data), IIB structure
*   Output: void
*/
void update_inverter_actual_values_2(IIB *iib, int inverter_num, uint16_t message_data[4]){

    if (inverter_num != 0 && inverter_num != 1 && inverter_num != 2 && inverter_num != 3) {
        line = 37;
        return;
    }

    iib->inverter[inverter_num].temp_motor = message_data[0];  
    line = 38;         /// Motor temperature
    iib->inverter[inverter_num].temp_inverter = message_data[1]; 
    line = 39;       /// Inverter temperature
    iib->inverter[inverter_num].error_info = message_data[2]; 
    line = 40;          /// Info about motor error
    iib->inverter[inverter_num].temp_IGBT = message_data[3];            /// IGBT transistor temperature       

    line = 13;

	return;
}


/**
*   Name: process_amk_message
*   Description: discovers which message we recieved, from which inverter it was, and updates the information
*   Input's: The received CAN message(msg), IIB structure for updating info
*   Output: void
*/
void process_amk_message(IIB *iib, CANdata msg){

    send_can2(msg);

    if (iib == NULL) {
        return;
    }
    
    int inverter = 0;   /** variable to identify the inverter */

    inverter = transform_id_to_inverter(iib, msg);

    /** we discover the inverter number */
    if (inverter == -1) { 
        return;
    }
    /* Check if it is message 1 and what inverter sent it
     * AMK Actual values 1 - consult AMK manual
     */

    if(msg.sid == iib->inverter[inverter].message1_id){

        update_inverter_actual_values_1(iib, inverter, msg.data);   /* update message 1 info */
    
        msg.msg_id = MSG_ID_IIB_AMK_ACTUAL_VALUES_1;
    }


    /// Check if it is message 2 and what inverter send it
    else if(msg.sid == iib->inverter[inverter].message2_id){ 

        update_inverter_actual_values_2(iib, inverter, msg.data);   /** update message 2 info */
    
         msg.msg_id = MSG_ID_IIB_AMK_ACTUAL_VALUES_2;
    }               

    /// Clear wtd timer for the inverter that sent the message
    // switch(inverter){ 
                
    //     case 0: wtd_FL = 0 ;break;    /** FL */
    //     case 1: wtd_FR = 0 ;break;    /** FR */
    //     case 2: wtd_RL = 0 ;break;    /** RL */
    //     case 3: wtd_RR = 0 ;break;    /** RR */ 
    // }

    msg.dev_id = inverter;

    send_can2(msg);

    return;
}

void set_turn_on(IIB *iib, int inverter){

    if (inverter != 0 && inverter != 1 && inverter != 2 && inverter != 3) {
        return;
    }

    iib->inverter[inverter].setpoints.control_word.b_inverter_on = 1;
    iib->inverter[inverter].setpoints.control_word.b_dc_on = 1;
    iib->inverter[inverter].setpoints.control_word.b_enable = 1;
    iib->inverter[inverter].setpoints.control_word.b_error_reset = 0;

    return;
}


/**
*   Name: set_turn_on
*   Description: prepares for the turn on sequence
*   Input's: IIB structure containing the control word
*   Output: void
*/
void set_turn_on_all(IIB *iib){

    int i = 0;

    for(i = 0; i < 4; i++){ 

        if(iib->inverter[i].inverter_active == 1){  /** Verify if inverter is active */
        
            /** Controll word for turn on */
            set_turn_on(iib, i);
        }
    }

	return;
}

    
/**
*   Name: set_reset
*   Description: resets 1 inverter
*   Input's: IIB structure containing the inverter setpoints and the respective control word
*   Output: void
*/ 
void set_reset(IIB *iib, int inverter){
    
    /*if (inverter != 0 && inverter != 1 && inverter != 2 && inverter != 3) {
        return;
    }*/

    /// Set idle
    iib->inverter[inverter].setpoints.torque_limit_positive = 0;
    iib->inverter[inverter].setpoints.torque_limit_negative = 0;
    iib->inverter[inverter].setpoints.target_setpoint = 0;
        

    /** Controll word for reset */
    iib->inverter[inverter].setpoints.control_word.b_inverter_on = 0;
    iib->inverter[inverter].setpoints.control_word.b_dc_on = 0;
    iib->inverter[inverter].setpoints.control_word.b_enable = 0;
    iib->inverter[inverter].setpoints.control_word.b_error_reset = 1;       

	return;
}

/**
*   Name: set_reset_all
*   Description: resets all inverters
*   Input's: IIB structure containing the inverter setpoints and the respective control word
*   Output: void
*/ 
void set_reset_all(IIB *iib){

    int i = 0;

    for(i = 0; i < 4; i++){

        set_reset(iib, i);
    }

	return;
}
