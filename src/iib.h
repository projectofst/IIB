#ifndef __IIB_H__
#define __IIB_H__
		
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "lib_pic33e/can.h"

#include "can-ids/Devices/IIB_CAN.h"

/// Base adresses for the 3 amk messages (see AMK Datasheet!)
#define BASE_ADRESS_ACTUAL_V1 	0x282
#define BASE_ADRESS_ACTUAL_V2	0x284
#define BASE_ADRESS_SETPOINTS   0x183


/// Node adress for each inverter 
#define NODE_FL  0x0 
#define NODE_FR  0x1
#define NODE_RL  0x4
#define NODE_RR  0x5

/// AMK motor controll parameters 
#define NOMINAL_TORQUE 			9.8		//Units: N*m
#define MOTOR_KT				0.26	//Units: N*m/Arms
#define CONVERTER_PEAK_CURRENT	107.2	//Units: [A], ID 110	(not used)
#define MOTOR_NOMINAL_CURRENT 	41		//Units: [A], ID 111
#define MOTOR_PEAK_CURRENT		109		//Units: [A], ID 109	(not used)
#define TIME_MAX_CURRENT_MOTOR  1.24	//Units: [s], ID34168	(not used)

/// IIB operation modes
#define ARM_MODE 	 	0
#define DEFAULT_MODE 	1
#define TEST_BENCH_MODE 2
#define ERROR_MODE		3 

/// AMK controll modes
#define SPEED_CONTROL	0
#define TORQUE_CONTROL	1
#define CONTROL_MODE SPEED_CONTROL	/// default


#define I2T_VOLUME 16000	/** Default volume */

#define ADD(X,Y) (X)+(Y) 


/// Controll word for the inverters, contains all the status bits we want to modify
typedef struct {
	
	union {
		
		struct {
			
			unsigned b_reserve          :8;	/** not used */
			unsigned b_inverter_on      :1;
			unsigned b_dc_on            :1;
			unsigned b_enable           :1;
			unsigned b_error_reset      :1;
			unsigned b_reserve1         :4;	/** not used */
		};
		
		uint16_t word;
	};

}AMK_Control_word;


/// Status bit array that we recieve, 
/// Similar to the control word but only tells us the inverter status
typedef struct {
			
	union {

		struct {	
					
			unsigned reserved          :8;	/** not used */
			unsigned system_ready      :1;
			unsigned error             :1;
			unsigned warn              :1;
			unsigned quit_dc_on        :1;
			unsigned dc_on             :1;	
			unsigned quit_inverter_on  :1;
			unsigned inverter_on       :1;
			unsigned derating          :1;	
		};
				
		uint16_t word;
	};

}AMK_status;


/// Represents all the parameters of the AMK setpoints message
typedef struct {
		
	AMK_Control_word control_word;	

	int16_t target_setpoint;				/** Speed we want to achieve */
	uint16_t torque_limit_positive;		/** The torque we want the motor to produce (positive) */
	int16_t torque_limit_negative;		/** The torque we want the motor to produce (negative) */
	
}AMK_Setpoints;


/// Represents the car status, important info about the car will be stored in this structure
typedef struct {

	union {

		struct {
		
			unsigned te_ok		   	  :1;	/** every thing is ok with the Torque encoder */ 
			unsigned arm_ok		   	  :1;	/** every thing is ok with the ARM ccontroller */
		    unsigned arm_stupid       :1;	/** Arm is sending stupid message and we ignored ir */
		    unsigned arm_received_msg :1;	/** Indicates we received an arm setpoints message */
		    unsigned sdc1          	  :1;	/** status of the shutdown circuit 1 */
		    unsigned sdc2          	  :1;	/** status of the shutdown circuit 2 */
		    unsigned car_rtd       	  :1;	/** if 1 the rest of the car is "ready to drive" */
		  	unsigned inv_hv			  :1;	/** inverters have High voltage */
			unsigned tsal_status	  :1;
		};
		 
		uint16_t word;	
	};
	
}CarStatus;


/// Status word containing all temperature status about the inverter pack
typedef struct {
	
	union {
		
		struct {
			
			/// Indicates if motor is in derating, and what derating caused it
			unsigned inverter_derating_on	:1;
			unsigned inverter_max_passed	:1;
			
			unsigned IGBT_derating_on		:1;
			unsigned IGBT_max_passed		:1;
			
			unsigned motor_derating_on		:1;
			unsigned motor_max_passed		:1;
		
			unsigned i2t_overload_active	:1;	/** Motor as entered i2t time window */
			unsigned i2t_cooldown_on		:1;//i2t_cooldown_active	:1;	/** Motor is cooling down, limited to nominal current */
		};
		
		uint8_t word;
	};

}TempStatus;


/// Each inverter torque and power limit
typedef struct{

	/// Power:
	uint16_t max_safety_power;
	uint16_t max_operation_power;
	
	/// Torque:
	uint16_t max_safety_torque;
	uint16_t max_operation_torque;
	
	/** Regen torque */
	int16_t max_safety_regen_torque;
	int16_t max_operation_regen_torque;

}InverterLimits;



/**************************************************************************************************//**
*										MAIN STRUCTURES					 						      *
******************************************************************************************************/


/***************************//**
 Inverter
*******************************/		
/// Represents one inverter in the car, contains info about the interaction with the motors and the car
typedef struct {
	
	/// Information about id's
	unsigned node_address;			
	unsigned message1_id;
	unsigned message2_id;
	unsigned reference_id;
		   
	/// About inverter
	int16_t temp_inverter;
	int16_t temp_IGBT;
	uint16_t error_info;
	unsigned BE_2    			  :1;
	unsigned BE_1 				  :1;
	unsigned inverter_active      :1;	/** if 1 we want the inverter on */

	/// About associated motor
	int16_t actual_speed;
    int16_t torque_current;
   	int16_t magnetizing_current;
	int16_t temp_motor;
	int16_t torque;
    int16_t current;

	AMK_status status;			/** Inverter status word */
	
	AMK_Setpoints setpoints;	/** Setpoints to the associated motor */

    int same_axis_inverter;

}AMK_Inverter;



/***************************//**
 CAR
*******************************/		
/// As all information about the car, the status and other properties and more specific details about what the car is doing
typedef struct {

	uint16_t te_percentage;
	uint16_t bms_voltage;

	CarStatus status;

	int te_alive_again;

}Car;



/***************************//**
 Safety
*******************************/		
/// Contains all safety parameters to check about the car and inverters
typedef struct {

	//unsigned power_derating		:1;		/** indicates if we are in power derating */	(TO BE USED)!!!
	InverterLimits inverter_limits[4];
	TempStatus temp_status[4];				/** temperature derating status */
	int16_t max_rpm;
	
	/// i2t controll, 1 parameter per inverter
	uint16_t max_i2t_current[4];
	float i2t_saturation_time[4];
	float cool_down_on_time[4];


	float i2t_volume[4];
	int16_t previous_i2t_current[4];
	uint16_t i2t_trigger_volume;
	float i2t_scale_factor;

	int error_latch[4];

}Safety;

typedef struct {

	uint16_t inv_msg_read_error_numb;
	
	/// For debugging show deratings
	uint16_t derating_torque_operation[4];
	uint16_t derating_torque_safety[4];
	uint16_t temperature_derating[4];	
	uint16_t voltage_derating_torque[4]; 
	uint16_t total_power[4]; 
	uint16_t i2t_derating[4];

}Debug;

typedef struct {
	uint16_t *p;
	uint16_t max;
	uint16_t min;
	uint16_t dflt;
} Settables;

/***************************//**
 IIB (Inverter Interface Board)
*******************************/		
/// Contains every thing the iib needs to controll, 4 inverters, the car input and also variables related to the IIB, for example its temperature 
typedef struct {

	int input_mode;					/** IIB operating mode */
	int arm_input_mode;				/** ARM currente controll mode */
	bool debug_mode;		        /** Mode to get extra code information*/
	bool amk_control_mode;

	int16_t  air_temp;				/** Ambient IIB temperature */
	int16_t  discharge_temp;		/** Discharge circuit temperature */			
	uint16_t fan_speed;				/** Speed of the fan that controlls the iib temperature */
	
	/// Status
	bool fan_on				;
	bool cu_power_a			;
	bool cu_power_b			;
	bool inv_rtd			;		/** If inverters are ready */
	bool inv_on         	;		/** Inverters are on */
	bool turning_on			;		/** CAR is turning on */
    bool turning_off   		;		/** CAR is turning off */
    bool regen_on			;		/** Regeneration alowed */
    bool front_axis_status	;
    bool rear_axis_status	;
    bool allow_reset_errors	;
    bool arm_wtd_exceded	;

    int arm_message_number;

	AMK_Inverter inverter[4];		/** The 4 inverters in the inverter pack */
	Car car;
	Safety iib_safety;

	uint16_t version[4];
	//Debug iib_debug;

	Settables sets[IIB_PARAMETER_NUMBER];

}IIB;



/// Functions

void send_to_amk (IIB * iib);
void set_idle(IIB *iib, int inverter);
void set_idle_all(IIB * iib);
void set_turn_off(IIB *iib, int inverter);	 
void set_turn_off_all(IIB *iib);
void set_turn_on(IIB *iib, int inveter);
void set_turn_on_all(IIB *iib);
void set_reset(IIB *iib, int inverter);
void set_reset_all(IIB *iib);
void convert_torque_unit(IIB *iib);
void calculate_torque(IIB *iib, int inverter);
void calculate_current(IIB *iib, int inverter);
void update_inverter_actual_values_1(IIB *iib, int inverter_num, uint16_t message_data[4]);
void update_inverter_actual_values_2(IIB *iib, int inverter_num, uint16_t message_data[4]);
void process_amk_message(IIB * ibb, CANdata msg);

#endif 
