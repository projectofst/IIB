// FST Lisboa
// Project Template

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF				// General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF				// General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF				// General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC				// Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF				// Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS				// Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF			// OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF			// Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD			// Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768		// Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128			// Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON				// PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF				// Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = OFF			// Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4		        // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON				// Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF			// Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE				// ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF				// Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF				// JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF				// Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF				// Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF				// Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

// NOTE: Always include timing.h
#include "lib_pic33e/timing.h"
#include "lib_pic33e/usb_lib.h"
#include "lib_pic33e/pps.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/flash.h"
#include "lib_pic33e/version.h"
#include "lib_pic33e/trap.h"
#include <xc.h>

#include "main.h"
#include "safety.h"
#include "car.h"
#include "io.h"
#include "acceleration.h"

/// Drive modes
#define ALL_WHEEL_DRIVE 		1
#define TWO_WHEEL_DRIVE_REAR	2
#define TWO_WHEEL_DRIVE_FRONT 	3
#define ISOLATE_FL				4
#define ISOLATE_FR				5
#define ISOLATE_RL				6
#define ISOLATE_RR				7

#define DRIVE_MODE ALL_WHEEL_DRIVE	///Default mode
#define N_MOTOR 4

volatile unsigned amk_timer;
volatile uint16_t iib_tasks_timer;
volatile uint16_t non_priority_tasks_timer;


/*
 * wtd_arm is the watchdog for missing messages from arm
 * wtd_torque_encoder is the watchdog for missing messages from TE
 * good_arm_msg_timer is the wathcdog for missing "good condition "arm messages
 */
uint32_t wtd_arm;
uint32_t wtd_revive_arm;
uint32_t wtd_torque_encoder;
uint32_t good_arm_msg_timer;
uint32_t time;

/// Timer for when TE returns to life (to exit error mode to Default mode)
uint32_t te_alive_timer;

static IIB iib;

uint16_t line;

/**
*	Name: init_safety;
*	Description: Inits safety parameters
*	Input's: IIB structure
*	Output: void
*/
void init_safety(IIB *iib){

	int i = 0;

	for(i = 0; i < 4; i++){

		/// temperature related derating start at 0
		iib->iib_safety.temp_status[i].word = 0;

		/// i2t parameters
		iib->iib_safety.max_i2t_current[i]  	= 0;
		iib->iib_safety.i2t_saturation_time[i] 	= 0.0;
		iib->iib_safety.cool_down_on_time[i]	= 0.0;

		iib->iib_safety.i2t_volume[i] = 0.0;
		iib->iib_safety.previous_i2t_current[i] = 0;
		iib->iib_safety.error_latch[i] = 0;
	}

	/// FL
	iib->iib_safety.inverter_limits[0].max_safety_power 			= MAX_MOTOR_POWER_SAFETY_FL;
	iib->iib_safety.inverter_limits[0].max_operation_power 			= MAX_MOTOR_POWER_OP_FL;
	iib->iib_safety.inverter_limits[0].max_safety_torque 			= MAX_MOTOR_TORQUE_SAFETY_FL;
	iib->iib_safety.inverter_limits[0].max_operation_torque 		= MAX_MOTOR_TORQUE_OP_FL;
	iib->iib_safety.inverter_limits[0].max_safety_regen_torque		= MAX_MOTOR_REGEN_TORQUE_SAFETY_FL;
	iib->iib_safety.inverter_limits[0].max_operation_regen_torque	= MAX_MOTOR_REGEN_TORQUE_OP_FL;

	/// FR
	iib->iib_safety.inverter_limits[1].max_safety_power 			= MAX_MOTOR_POWER_SAFETY_FR;
	iib->iib_safety.inverter_limits[1].max_operation_power 			= MAX_MOTOR_POWER_OP_FR;
	iib->iib_safety.inverter_limits[1].max_safety_torque 			= MAX_MOTOR_TORQUE_SAFETY_FR;
	iib->iib_safety.inverter_limits[1].max_operation_torque 		= MAX_MOTOR_TORQUE_OP_FR;
	iib->iib_safety.inverter_limits[1].max_safety_regen_torque		= MAX_MOTOR_REGEN_TORQUE_SAFETY_FR;
	iib->iib_safety.inverter_limits[1].max_operation_regen_torque	= MAX_MOTOR_REGEN_TORQUE_OP_FR;

	/// RL
	iib->iib_safety.inverter_limits[2].max_safety_power 			= MAX_MOTOR_POWER_SAFETY_RL;
	iib->iib_safety.inverter_limits[2].max_operation_power 			= MAX_MOTOR_POWER_OP_RL;
	iib->iib_safety.inverter_limits[2].max_safety_torque 			= MAX_MOTOR_TORQUE_SAFETY_RL;
	iib->iib_safety.inverter_limits[2].max_operation_torque 		= MAX_MOTOR_TORQUE_OP_RL;
	iib->iib_safety.inverter_limits[2].max_safety_regen_torque		= MAX_MOTOR_REGEN_TORQUE_SAFETY_RL;
	iib->iib_safety.inverter_limits[2].max_operation_regen_torque	= MAX_MOTOR_REGEN_TORQUE_OP_RL;

	/// RR
	iib->iib_safety.inverter_limits[3].max_safety_power 			= MAX_MOTOR_POWER_SAFETY_RR;
	iib->iib_safety.inverter_limits[3].max_operation_power 			= MAX_MOTOR_POWER_OP_RR	;
	iib->iib_safety.inverter_limits[3].max_safety_torque 			= MAX_MOTOR_TORQUE_SAFETY_RR;
	iib->iib_safety.inverter_limits[3].max_operation_torque 		= MAX_MOTOR_TORQUE_OP_RR;
	iib->iib_safety.inverter_limits[3].max_safety_regen_torque		= MAX_MOTOR_REGEN_TORQUE_SAFETY_RR;
	iib->iib_safety.inverter_limits[3].max_operation_regen_torque	= MAX_MOTOR_REGEN_TORQUE_OP_RR;

	/// Speed and I2t volume initialization
	iib->iib_safety.max_rpm = DEFAULT_RPM;//MAX_RPM;
	iib->iib_safety.i2t_trigger_volume = I2T_VOLUME;
	iib->iib_safety.i2t_scale_factor = 0.0;

	return;
}

/**
*	Name: init_inverter;
*	Description: Inits inverter parameters
*	Input's: IIB structure
*	Output: void
*/
void init_inverter(IIB *iib){

	/// Message id's

	/// FL
	iib->inverter[0].node_address = NODE_FL;
	iib->inverter[0].message1_id  = BASE_ADRESS_ACTUAL_V1 + NODE_FL;
	iib->inverter[0].message2_id  = BASE_ADRESS_ACTUAL_V2 + NODE_FL;
	iib->inverter[0].reference_id = BASE_ADRESS_SETPOINTS + NODE_FL;

	iib->inverter[0].same_axis_inverter = 1;

	/// FR
	iib->inverter[1].node_address = NODE_FR;
	iib->inverter[1].message1_id  = BASE_ADRESS_ACTUAL_V1 + NODE_FR;
	iib->inverter[1].message2_id  = BASE_ADRESS_ACTUAL_V2 + NODE_FR;
	iib->inverter[1].reference_id = BASE_ADRESS_SETPOINTS + NODE_FR;

	iib->inverter[1].same_axis_inverter = 0;

	/// RL
	iib->inverter[2].node_address = NODE_RL;
	iib->inverter[2].message1_id  = BASE_ADRESS_ACTUAL_V1 + NODE_RL;
	iib->inverter[2].message2_id  = BASE_ADRESS_ACTUAL_V2 + NODE_RL;
	iib->inverter[2].reference_id = BASE_ADRESS_SETPOINTS + NODE_RL;

	iib->inverter[2].same_axis_inverter = 3;

	/// RR
	iib->inverter[3].node_address = NODE_RR;
	iib->inverter[3].message1_id  = BASE_ADRESS_ACTUAL_V1 + NODE_RR;
	iib->inverter[3].message2_id  = BASE_ADRESS_ACTUAL_V2 + NODE_RR;
	iib->inverter[3].reference_id = BASE_ADRESS_SETPOINTS + NODE_RR;

	iib->inverter[3].same_axis_inverter = 2;

	int i = 0;

	for(i = 0; i < 4; i++){

		/// Inverters
		iib->inverter[i].temp_inverter = 0;
		iib->inverter[i].temp_IGBT = 0;
		iib->inverter[i].error_info = 0;
		iib->inverter[i].BE_2 = 0;
		iib->inverter[i].BE_1 = 0;
		iib->inverter[i].inverter_active = 0;

		/// Motors
		iib->inverter[i].actual_speed = 0;
	    iib->inverter[i].torque_current = 0;
	   	iib->inverter[i].magnetizing_current = 0;
	   	iib->inverter[i].torque = 0;
	   	iib->inverter[i].current = 0;
		iib->inverter[i].temp_motor = 0;
		iib->inverter[i].status.word = 0;

		/// Setpoints
		iib->inverter[i].setpoints.target_setpoint = 0;
		iib->inverter[i].setpoints.torque_limit_positive = 0;
		iib->inverter[i].setpoints.torque_limit_negative = 0;
		iib->inverter[i].setpoints.control_word.word = 0;
	}

	/// Decide drive mode
	if(DRIVE_MODE == ALL_WHEEL_DRIVE){

		for(i = 0; i < N_MOTOR; i++){

			iib->inverter[i].inverter_active = 1;
		}
	}
	if(DRIVE_MODE == TWO_WHEEL_DRIVE_REAR){

		iib->inverter[2].inverter_active = 1;
		iib->inverter[3].inverter_active = 1;
	}
	if(DRIVE_MODE == TWO_WHEEL_DRIVE_FRONT){

		iib->inverter[0].inverter_active = 1;
		iib->inverter[1].inverter_active = 1;
	}
	if(DRIVE_MODE == ISOLATE_FL){

		iib->inverter[0].inverter_active = 1;
	}
	if(DRIVE_MODE == ISOLATE_FR){

		iib->inverter[1].inverter_active = 1;
	}
	if(DRIVE_MODE == ISOLATE_RL){

		iib->inverter[2].inverter_active = 1;
	}
	if(DRIVE_MODE == ISOLATE_RR){

		iib->inverter[3].inverter_active = 1;
	}

	return;
}


/**
*	Name: init_car;
*	Description: Inits car parameters
*	Input's: IIB structure
*	Output: void
*/
void init_car(IIB *iib){

	/// CAR status
	iib->car.status.word = 0;

	/// CAR parameters
	iib->car.te_percentage = 0;
	iib->car.bms_voltage = 0;
	iib->car.te_alive_again = 0;

	return;
}


/**
*	Name: timer1_callback;
*	Description: timer1 interrupt, amk timer, all i2t timers
*	Input's: void
*	Output: void
*/


void timer1_callback(){

	time++;
	wtd_arm++;
	amk_timer++;
	wtd_torque_encoder++;
	wtd_revive_arm++;

	return;
}


/**
*	Name: timer2_callback;
*	Description: timer2 interrupt, ARM Watchdog, TE watchdog
*	Input's: void
*	Output: void
*/
/*void timer2_callback(){

	iib_tasks_timer++;

     //if(good_arm_msg_timer < 17){

     //	good_arm_msg_timer++;
     //}

     //if(wtd_arm < 12){

		// wtd_arm++;
   	//}

   	//if(wtd_torque_encoder < 3){

   	wtd_torque_encoder++;
   	//}

   	te_alive_timer++;
}*/

/**
*	Name: timer3_callback;
*	Description: timer3 interrupt, amk timer, all i2t timers
*	Input's: void
*	Output: void
*/
/*void timer3_callback(){

	if(non_priority_tasks_timer < 20){

		non_priority_tasks_timer++;
	}
}*/


/**
*	Name: read_maximum;
*	Description: reads a maximum value from memory, if reading is wrong update with default value
*	Input's: void
*	Output: void
*/
void read_maximum(uint16_t default_max, uint16_t *variable, uint16_t parameter){

	FlashError erro = 0;
	uint16_t buffer;

	erro = read_flash (&buffer, parameter, 1);

	/** If there is an error we update with the default defined values */
	if(erro != 0){

		*variable = default_max;
	}
	/** If there is no stored value it returns al 1's, update with default value **/
	else if(buffer == 65535){

		*variable = default_max;
	}
	/** Every thing ok, we update from memory */
	else if(erro == 0){

		*variable = buffer;
	}

	return;
}


/**
*	Name: flash_read;
*	Description: IIB structure
*	Input's: void
*	Output: void
*/
void iib_flash_read(IIB *iib){

	uint16_t buffer;
	FlashError erro = 0;

	/** Check if we enabled regen */
	erro = read_flash (&buffer, P_IIB_REGEN, 1);

	if(erro != 0){	/// If there is an error while reading we dont enable regen
		iib->regen_on = 0;
	}
	else if(erro == 0){  // No error we update the value
		iib->regen_on = buffer;
	}

	/** Read all maximum operation values stored in memory */
	read_maximum(MAX_MOTOR_TORQUE_OP_FL,
			&(iib->iib_safety.inverter_limits[0].max_operation_torque),
			P_IIB_FL_MAX_TORQUE_OP);
	read_maximum(MAX_MOTOR_TORQUE_OP_FR,
			&(iib->iib_safety.inverter_limits[1].max_operation_torque),
			P_IIB_FR_MAX_TORQUE_OP);
	read_maximum(MAX_MOTOR_TORQUE_OP_RL,
			&(iib->iib_safety.inverter_limits[2].max_operation_torque),
			P_IIB_RL_MAX_TORQUE_OP);
	read_maximum(MAX_MOTOR_TORQUE_OP_RR,
			&(iib->iib_safety.inverter_limits[3].max_operation_torque),
			P_IIB_RR_MAX_TORQUE_OP);
	read_maximum(MAX_MOTOR_POWER_OP_FL,
			&(iib->iib_safety.inverter_limits[0].max_operation_power),
			P_IIB_FL_MAX_POWER_OP);
	read_maximum(MAX_MOTOR_POWER_OP_FR,
			&(iib->iib_safety.inverter_limits[1].max_operation_power),
			P_IIB_FR_MAX_POWER_OP);
	read_maximum(MAX_MOTOR_POWER_OP_RL,
			&(iib->iib_safety.inverter_limits[2].max_operation_power),
			P_IIB_RL_MAX_POWER_OP);
	read_maximum(MAX_MOTOR_POWER_OP_RR,
			&(iib->iib_safety.inverter_limits[3].max_operation_power),
			P_IIB_RR_MAX_POWER_OP);


	/** Check if we enabled debug mode */
	erro = read_flash (&buffer, P_IIB_DEBUG_MODE , 1);

	if(erro != 0){	/// If there is an error while reading we dont enable debug
		iib->debug_mode = 0;
	}
	else if(erro == 0){  // No error we update the value
		iib->debug_mode = buffer;
	}

	/** I2T scale factor */
	erro = read_flash (&buffer, P_IIB_I2T_SCALE_FACTOR , 1);

	if(erro != 0){	/// If there is an error while reading we dont enable debug
		iib->iib_safety.i2t_trigger_volume =  I2T_VOLUME * (float)(buffer);
	}
	else if(erro == 0){  // No error we update the value
		iib->iib_safety.i2t_trigger_volume =  I2T_VOLUME;
	}

	erro = read_flash(&buffer, P_IIB_MAX_RPM, 1);

	if(erro != 0){
		iib->iib_safety.max_rpm = buffer;
	}
	else if(erro == 0){
		iib->iib_safety.max_rpm = MAX_RPM;
	}

	return;

}


/*bool filter_can2(unsigned int sid){

	if(sid == 1229 || sid == 329 || sid == 494 || sid == 1966|| CAN_GET_MSG_ID(sid) == 17 || CAN_GET_MSG_ID(sid) == 16 || CAN_GET_MSG_ID(sid) == 27){

		return 1;
	}
	else{

		return 0;
	}

}*/


/**
*	Name: init_iib;
*	Description: init all iib parameters, output stages etc...
*	Input's: IIB structure
*	Output: void
*/
void init_iib(IIB *iib){

	// Never remove this, prevents resets
	INTCON1bits.NSTDIS = 1;

	VERSIONerror version_error;
	uint16_t buffer;

	/// Init iib parameters
	iib->air_temp 			= 0;
	iib->discharge_temp 	= 0;
	iib->fan_speed 			= 0;
	iib->fan_on 			= 0;
	iib->cu_power_a	 		= 0;
	iib->cu_power_b 		= 0;
	iib->inv_rtd 			= 0;
	iib->inv_on    			= 0;
	iib->turning_on			= 0;
    iib->turning_off   		= 0;
    iib->regen_on			= 1;
    iib->front_axis_status 	= 1;
    iib->rear_axis_status 	= 1;
    iib->allow_reset_errors	= 1;
    iib->arm_wtd_exceded	= 0;
    iib->arm_message_number = 0;

	/// Initial ARM mode(ARM updates this)
	iib->arm_input_mode = 0;

	iib->debug_mode = 1;
	iib->amk_control_mode = CONTROL_MODE;

	/// Config io pins
	io_config();

	/// Unlock CAN pins
	PPSUnLock;
	PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118);
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119);
    PPSOutput(OUT_FN_PPS_C2TX, OUT_PIN_PPS_RP120);
    PPSInput(IN_FN_PPS_C2RX, IN_PIN_PPS_RPI121);
    PPSLock;

    CANconfig config;
    default_can_config(&config);
    config.interrupt_priority = 4;
    config_can1(NULL);
	config_can2(NULL);

	config_timer1(20,5); //choose time and priority (1ms)
    //config_timer2(400,5); //choose time and priority (10ms)
    //config_timer3(250,6); //chosse time and priority (100ms)

	init_safety(iib);
	init_inverter(iib);
	init_car(iib);


	/** Get git version code */
	version_error = get_version(VERSION, iib->version);

	if(version_error != VSUCCESS){

	 	iib->version[0] = 0;
	 	iib->version[1] = 0;
	 	iib->version[2] = 0;
	 	iib->version[3] = 0;
	}


	/** Reads all parameters stored in flash */
	//iib_flash_read(iib);

	/** Turn on control units power */
    EF_B_WRITE = 1;
	EF_A_WRITE = 1;
	iib->cu_power_a = EF_A_WRITE;
	iib->cu_power_b = EF_B_WRITE;

	/** Turn on individual inverters */

	/// INV FL
    if(iib->inverter[0].inverter_active == 1){

    	BE2_INV1_WRITE = 1;
        BE1_INV1_WRITE = 1;
        iib->inverter[0].BE_2 = BE2_INV1_WRITE;
        iib->inverter[0].BE_1 = BE1_INV1_WRITE;
   	}
    /// INV FR
    if(iib->inverter[1].inverter_active == 1){

        BE2_INV2_WRITE = 1;
        BE1_INV2_WRITE = 1;
        iib->inverter[1].BE_2 = BE2_INV2_WRITE;
        iib->inverter[1].BE_1 = BE1_INV2_WRITE;
    }
    /// INV RL
    if(iib->inverter[2].inverter_active == 1){

        BE2_INV3_WRITE = 1;
        BE1_INV3_WRITE = 1;
        iib->inverter[2].BE_2 = BE2_INV3_WRITE;
        iib->inverter[2].BE_1 = BE1_INV3_WRITE;
    }
    /// INV RR
    if(iib->inverter[3].inverter_active == 1){

        BE2_INV4_WRITE = 1;
        BE1_INV4_WRITE = 1;
        iib->inverter[3].BE_2 = BE2_INV4_WRITE;
        iib->inverter[3].BE_1 = BE1_INV4_WRITE;
    }

    /// Initial IIB mode
    iib->input_mode = ARM_MODE;
    wtd_arm = 0;

	int i=0;
	for (i=0; i<IIB_PARAMETER_NUMBER; i++) {
		iib->sets[i].p = NULL;
		iib->sets[i].max = 65535;
		iib->sets[i].min = 0;
		iib->sets[i].dflt = 0;
	}

	iib->sets[0].p  = &iib->version[0];
	iib->sets[1].p  = &iib->version[1];
	iib->sets[2].p  = &iib->version[2];
	iib->sets[3].p  = &iib->version[3];

	iib->sets[4].p  = &iib->regen_on;
	iib->sets[4].max = 1;

	iib->sets[5].p  = &iib->iib_safety.inverter_limits[0].max_operation_torque;
	iib->sets[5].max = MAX_MOTOR_TORQUE_SAFETY_FL;

	iib->sets[6].p  = &iib->iib_safety.inverter_limits[1].max_operation_torque;
	iib->sets[6].max = MAX_MOTOR_TORQUE_SAFETY_FR;

	iib->sets[7].p  = &iib->iib_safety.inverter_limits[2].max_operation_torque;
	iib->sets[7].max = MAX_MOTOR_TORQUE_SAFETY_RL;

	iib->sets[8].p  = &iib->iib_safety.inverter_limits[3].max_operation_torque;
	iib->sets[8].max = MAX_MOTOR_TORQUE_SAFETY_RR;

	iib->sets[9].p  = &iib->iib_safety.inverter_limits[0].max_operation_power;
	iib->sets[9].max = MAX_MOTOR_POWER_SAFETY_FL;

	iib->sets[10].p = &iib->iib_safety.inverter_limits[1].max_operation_power;
	iib->sets[10].max = MAX_MOTOR_POWER_SAFETY_FR;

	iib->sets[11].p = &iib->iib_safety.inverter_limits[2].max_operation_power;
	iib->sets[11].max = MAX_MOTOR_POWER_SAFETY_RL;

	iib->sets[12].p = &iib->iib_safety.inverter_limits[3].max_operation_power;
	iib->sets[12].max = MAX_MOTOR_POWER_SAFETY_RR;

	iib->sets[13].p = &iib->debug_mode;

	iib->sets[14].p = &iib->iib_safety.i2t_scale_factor;

	iib->sets[15].p = &iib->input_mode;
	iib->sets[15].max = 3;

	iib->sets[16].p = &iib->iib_safety.max_rpm;
	iib->sets[16].max = MAX_RPM;

	iib->sets[17].p = &iib->iib_safety.inverter_limits[0].max_operation_regen_torque;
	iib->sets[17].max = MAX_MOTOR_REGEN_TORQUE_SAFETY_FL;

	iib->sets[18].p = &iib->iib_safety.inverter_limits[1].max_operation_regen_torque;
	iib->sets[17].max = MAX_MOTOR_REGEN_TORQUE_SAFETY_FR;

	iib->sets[19].p = &iib->iib_safety.inverter_limits[2].max_operation_regen_torque;
	iib->sets[17].max = MAX_MOTOR_REGEN_TORQUE_SAFETY_RL;

	iib->sets[20].p = &iib->iib_safety.inverter_limits[3].max_operation_regen_torque;
	iib->sets[17].max = MAX_MOTOR_REGEN_TORQUE_SAFETY_RR;

	iib->sets[21].p = &iib->amk_control_mode;
	iib->sets[21].max = 1;

	for (i=0; i<IIB_PARAMETER_NUMBER; i++) {
		read_flash(&buffer, i, 1);
		if (buffer == 65535) {
			continue;
		}
		*(iib->sets[i].p) = buffer;
	}

	return;
}


/**
*	Name: receive_messages;
*	Description: See in wich can line we have a message
*	Input's: IIB structure
*	Output: void
*/
void receive_messages(IIB * iib){

    CANdata msg;

    /// Check for can1:
    if(!can1_rx_empty()){

        msg = pop_can1();
        process_amk_message(iib, msg);
    }

    /// Check for can2:
    if(!can2_rx_empty()){

        msg = pop_can2();
      	process_car_message(iib, msg);
    }

	return;
}


/**
*	Name: send_motor_info;
*	Description: Send 4 can messages with motor information
*	Input's: IIB structure
*	Output: void
*/
void send_motor_info(IIB *iib){

    CANdata inv_info[4];
    int i = 0;

    for(i =0 ; i < N_MOTOR; i++){

        inv_info[i].dev_id = DEVICE_ID_IIB;
        inv_info[i].msg_id = MSG_ID_IIB_MOTOR_INFO;
        inv_info[i].dlc = 8;

        uint16_t aux_temp_motor = 0;
        aux_temp_motor = (iib->inverter[i].temp_motor/10) + 1;

        //fill can datas
        inv_info[i].data[0] = (i << 14) | (aux_temp_motor & 0b011111111);

        if(iib->inverter[i].temp_motor == 0){
        	inv_info[i].data[0] = (i << 14) | 0b000000000;
        }


        inv_info[i].data[1] = iib->inverter[i].actual_speed;
        inv_info[i].data[2] = iib->inverter[i].current;
        inv_info[i].data[3] = iib->inverter[i].torque;

        send_can2(inv_info[i]);
    }

	return;
}


/**
*	Name: send_inverter_limits;
*	Description: Send 4 can messages with inverter limits
*	Input's: IIB structure
*	Output: void
*/
void send_inverter_limits(IIB *iib){

	CANdata iib_limits[4];
	int i = 0;

	for(i = 0; i < N_MOTOR; i++){

		iib_limits[i].dev_id = DEVICE_ID_IIB;
        iib_limits[i].msg_id = MSG_ID_IIB_LIMITS;
        iib_limits[i].dlc = 8;

        iib_limits[i].data[0] = i << 14 | (uint8_t)(iib->iib_safety.inverter_limits[i].max_safety_power/1000.0);
        iib_limits[i].data[1] = iib->iib_safety.inverter_limits[i].max_operation_power;
        iib_limits[i].data[2] = iib->iib_safety.inverter_limits[i].max_safety_torque;
        iib_limits[i].data[3] = iib->iib_safety.inverter_limits[i].max_operation_torque;

        send_can2(iib_limits[i]);
	}

	return;
}

void send_inverter_regen_limits(IIB *iib){

	CANdata iib_regen_limits[4];
	int i = 0;

	for(i = 0; i < N_MOTOR; i++){

		iib_regen_limits[i].dev_id = DEVICE_ID_IIB;
		iib_regen_limits[i].msg_id = MSG_ID_IIB_REGEN_LIMITS;
		iib_regen_limits[i].dlc = 8;

		iib_regen_limits[i].data[0] = i;
        iib_regen_limits[i].data[1] = iib->iib_safety.inverter_limits[i].max_safety_regen_torque;
        iib_regen_limits[i].data[2] = iib->iib_safety.inverter_limits[i].max_operation_regen_torque;
        iib_regen_limits[i].data[3] = 0;

        send_can2(iib_regen_limits[i]);
	}
}

/**
*	Name: send_inverter_info;
*	Description: Send 4 can messages with inverter information
*	Input's: IIB structure
*	Output: void
*/
void send_inverter_info(IIB *iib){

	CANdata inv_info[4];
	int i = 0;

	for(i = 0; i < 4; i++){

		inv_info[i].dev_id = DEVICE_ID_IIB;
        inv_info[i].msg_id = MSG_ID_IIB_INV_INFO;
        inv_info[i].dlc = 8;

        inv_info[i].data[0] = iib->iib_safety.temp_status[i].word << 5 | i << 3 | iib->inverter[i].BE_2 << 2 | iib->inverter[i].BE_1 << 1 | iib->inverter[i].inverter_active;
        inv_info[i].data[1] = iib->inverter[i].temp_IGBT;
        inv_info[i].data[2] = iib->inverter[i].temp_inverter;
        inv_info[i].data[3] = iib->inverter[i].error_info;

        send_can2(inv_info[i]);
	}

	return;
}


/**
*	Name: send_iib_info;
*	Description: Send can message with iib information
*	Input's: IIB structure
*	Output: void
*/
void send_iib_info(IIB *iib){

	CANdata iib_info;

	iib_info.dev_id = DEVICE_ID_IIB;
    iib_info.msg_id = MSG_ID_IIB_INFO;
    iib_info.dlc = 8;

    iib_info.data[0] = iib->air_temp;
    iib_info.data[1] = iib->discharge_temp;
    iib_info.data[2] = iib->iib_safety.max_rpm;
    iib_info.data[3] = iib->fan_on | iib->cu_power_a << 1 | iib->cu_power_b << 2 | iib->inv_rtd << 3
    | iib->inv_on << 4 | iib->regen_on << 5 | iib->debug_mode << 6 | iib->iib_safety.error_latch[0] << 7
    | iib->iib_safety.error_latch[1] << 8 | iib->iib_safety.error_latch[2] << 9 | iib->iib_safety.error_latch[3] << 10;

    send_can2(iib_info);

	return;
}


/**
*	Name: send_car_info;
*	Description: Send can message with car information
*	Input's: IIB structure
*	Output: void
*/
void send_car_info(IIB * iib){

	CANdata car_info;

	car_info.dev_id = DEVICE_ID_IIB;
	car_info.msg_id = MSG_ID_IIB_CAR_INFO;
	car_info.dlc = 8;


	car_info.data[0] = iib->car.status.word;
	car_info.data[1] = iib->amk_control_mode;	///dividir por 100
	car_info.data[2] = iib->input_mode;
	car_info.data[3] = iib->car.bms_voltage;

	send_can2(car_info);

	return;
}


/**
*	Name: send_debug_msg;
*	Description: Send can message with debug information (message 1 and 2)
*	Input's: IIB structure
*	Output: void
*/
void send_debug_msg(IIB *iib){

	int i = 0;

	CANdata debug_msg[4];

	for(i = 0; i < N_MOTOR; i++){

		debug_msg[i].dev_id = DEVICE_ID_IIB;
		debug_msg[i].msg_id = MSG_ID_IIB_DEBUG1_INFO;
		debug_msg[i].dlc = 8;

		debug_msg[i].data[0] = i | iib->car.status.arm_received_msg << 2 | iib->inverter[i].node_address << 3 | iib->car.te_alive_again << 7;
		debug_msg[i].data[1] = iib->iib_safety.max_i2t_current[i];
		debug_msg[i].data[2] = iib->iib_safety.i2t_saturation_time[i];
		debug_msg[i].data[3] = iib->iib_safety.cool_down_on_time[i];

		send_can2(debug_msg[i]);
	}


	for(i = 0; i < N_MOTOR; i++){

		debug_msg[i].msg_id = MSG_ID_IIB_DEBUG2_INFO;

		debug_msg[i].data[0] = iib->inverter[i].status.word;
		debug_msg[i].data[1] = iib->inverter[i].message1_id;
		debug_msg[i].data[2] = iib->inverter[i].message2_id;
		debug_msg[i].data[3] = iib->inverter[i].reference_id;

		send_can2(debug_msg[i]);
	}

	return;
}

void send_status(IIB *iib){

	CANdata status_msg;

	status_msg.dev_id = 16;
	status_msg.msg_id = 16;
	status_msg.dlc = 8;
	status_msg.data[0] = iib->inverter[2].status.word;

	send_can2(status_msg);

	return;

}

void teo(IIB *iib){

	CANdata teo;
	float speed = 0;
	int i = 0;
	for(i = 0; i < 4; i++){

		speed += speed + iib->inverter[i].actual_speed;
	}

	speed = speed/4.0;

	teo.dlc = 2;
	teo.dev_id = DEVICE_ID_IIB;
	teo.msg_id = 60;
	teo.data[0] = (uint16_t)(speed);

	send_can2(teo);
}

void reset_msg(void){

	CANdata reset = make_reset_msg(DEVICE_ID_IIB, RCON);
	send_can2(reset);

	return;
}


int main(void){

	reset_msg();	/// Reset message, with RCON register

	/// Initialize iib parameters
	init_iib(&iib);

	LATDbits.LATD1 = 1;
	LATDbits.LATD2 = 0;

	while(1){

		verify_shut_circuit(&iib);
		verify_TSAL(&iib);
		receive_messages(&iib);

		if(amk_timer == 2){

		 	ez_safety(&iib);
		 	send_to_amk(&iib);

		 	amk_timer = 0;
		}

		/// Communication
		if(time == 50){

		 	LED1_WRITE ^= 1;
		 	LATDbits.LATD1 ^= 1;

		 	teo(&iib);
		 	send_car_info(&iib);
		 	send_iib_info(&iib);
		 	time = 54;

            uint16_t torque = get_accel_curve(&iib, 3);

            CANdata msg;
            msg.dev_id = 16;
            msg.msg_id = 62;
            msg.dlc = 8;
            msg.data[0] = torque;
            send_can2(msg);

		}
		if(time == 58){
			send_inverter_info(&iib);
		 	time = 62;
		}
		if(time == 66){
			send_motor_info(&iib);
		 	time = 70;
		}
		if(time == 74){
			send_inverter_limits(&iib);
			time = 78;
		}
		if(time == 82){
			send_inverter_regen_limits(&iib);
			time = 0;
		}

		ClrWdt();

		line = 3;

	}//End of while(1)

	return 0;
}



void trap_handler(TrapType type){

	CANdata trap;
	trap.dev_id = DEVICE_ID_IIB;
	trap.msg_id = MSG_ID_COMMON_TRAP;

	trap.dlc = 8;
	trap.data[0] = type;
	trap.data[1] = line;
	trap.data[2] = 0;
	trap.data[3] = 0;
	send_can2(trap);
	FAN_CONTROL_WRITE = 1;
	LATDbits.LATD2 = 1;
	__delay_ms(7000);

	return;
}

