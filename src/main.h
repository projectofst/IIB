#ifndef __MAIN1_H__
#define __MAIN1_H__

#include "iib.h"
#include "can-ids/Devices/IIB_CAN.h"

void read_maximum(uint16_t default_max, uint16_t *variable, uint16_t parameter);
void flash_read(IIB *iib);
void init_safety(IIB*iib);
void init_inverter(IIB *iib);
void init_car(IIB *iib);
void init_iib(IIB *iib);
void receive_messages(IIB * iib);
void send_inverter_limits(IIB *iib);
void send_motor_info(IIB *iib);
void send_inverter_info(IIB *iib);
void send_iib_info(IIB *iib);
void send_car_info(IIB * iib);

#endif