/*! \file
 *  \brief  Safety functions
 */

#include "safety.h"
#include "acceleration.h"

/** Computes the minimum of 2 values */
#define MIN(x,y) (((x) < (y)) ? (x):(y))

/// Extern variables
extern uint16_t wtd_arm;
extern uint32_t wtd_torque_encoder;
extern uint16_t good_arm_msg_timer;
extern uint32_t wtd_revive_arm;

extern uint16_t line;


/**
*	Name: verify_shut_circuit;
*	Description: Updates shutdown circuit status and turns inverters on if something is wrong
*	Input's: IIB structure
*	Output: void
*/
void verify_shut_circuit(IIB * iib){

	/// Second SDC detection point
	if(sdc2_READ){	/** Good */
		iib->car.status.sdc2 = 1;
	}
	else{	/** Not good */
		iib->car.status.sdc2 = 0;
	}

	/// First SDC detection point
	if(sdc1_READ){
		iib->car.status.sdc1 = 1;
	}
	else{	/** Not good */
		iib->car.status.sdc1 = 0;
	}

	/// Check if inverters are on, and SDC is open, turnoff
	if((sdc1_READ == 0 || sdc2_READ == 0) && iib->inv_on == 1){

		turn_off_sequence(WE_TURN_OFF, iib);
	}

	return;
}


/**
*	Name: derating_linear_form;
*	Description: Calulate y = y(x), linear equation
*	Input's: equation slope m, curve displacement in y axis, b, and the x value
*	Output: unsigned integer
*/
uint16_t derating_linear_form(float m, int b, uint16_t x){
	return m*x + b;
}


/**
*	Name: voltage_deratings;
*	Description: Applys deratings concerning tractive system voltage
*	Input's: IIB structure;
*	Output: Max torque if every thing is ok 21 N*m *1000, or the most limiting derating, meaning the lowest torque value
*/
uint16_t voltage_deratings(IIB *iib){

	uint16_t charge_derating 	= 21000;
	uint16_t discharge_derating = 21000;


	/// Verify if we are above Warning deep of charge voltage
	if(iib->car.status.inv_hv == 1 && iib->car.bms_voltage >= WARNING_DEEP_OF_CHARGE &&
		iib->car.bms_voltage <= MAX_VOLTAGE){

		charge_derating = derating_linear_form((-21.0/(MAX_VOLTAGE - WARNING_DEEP_OF_CHARGE)), 21,
			(iib->car.bms_voltage - WARNING_DEEP_OF_CHARGE));
	}
	/// Verify if we are below Warning deep of discharge voltage
	else if(iib->car.status.inv_hv == 1 && iib->car.bms_voltage <= WARNING_DEEP_OF_DISCHARGE &&
		iib->car.bms_voltage >= MIN_VOLTAGE){

		discharge_derating = derating_linear_form((21.0/(MIN_VOLTAGE - WARNING_DEEP_OF_DISCHARGE)), 21,
			(WARNING_DEEP_OF_DISCHARGE - iib->car.bms_voltage));
	}
	/// Verify if we are above Maximum alowed voltage
	else if(iib->car.status.inv_hv == 1 && iib->car.bms_voltage > MAX_VOLTAGE){

		charge_derating = 0;	/** No torque must be generated */
	}
	/// Verify if we are below Minimum voltage
	else if(iib->car.status.inv_hv == 1 && iib->car.status.inv_hv < MIN_VOLTAGE){

		discharge_derating = 0;		/** No torque must be generated */
	}


	/// Voltage is under normal operatin range
	if(charge_derating == 21000 && discharge_derating == 21000){

		return 21000;

	/// if not good, return discharge derating if it limits most
	}else if(discharge_derating <= charge_derating){

		return 1000*discharge_derating;		/** N*m *1000 */

	/// if not , return charge derating if it limits most
	}else if(charge_derating < discharge_derating){

		return 1000*charge_derating;	/** N*m *1000 */
	}

	return 0;	//** None of the conditions are meet, code is noot good */
}


/**
*	Name: temperature_deratings;
*	Description: Applys deratings concerning the 3 critical temperatures(Inverters, motors and IGBT)
*	Input's: motor number, IIB structure
*	Output: Max torque if every thing is ok 21 N*m *1000, or the most limiting derating, meaning the lowest torque value
*/
uint16_t temperature_deratings(int motor, IIB *iib){

	int i = motor;
	/** initialize with the max available torque */
	//uint16_t inverter_derating = 21000, igbt_derating = 21000, motor_temp_derating = 21000;

	/// Verify Inverter cold plate temperature
	if(iib->inverter[i].temp_inverter > WARNING_INVERTER_TEMP && iib->inverter[i].temp_inverter < MAX_INVERTER_TEMP){

		/// We entered derating
		iib->iib_safety.temp_status[i].inverter_derating_on = 1;
		/*inverter_derating = derating_linear_form((-21.0/(MAX_INVERTER_TEMP - WARNING_INVERTER_TEMP)), 21, (
			iib->inverter[i].temp_inverter - WARNING_INVERTER_TEMP));*/

	}else if(iib->inverter[i].temp_inverter >= MAX_INVERTER_TEMP){	/** We passed the max temperature */

		iib->iib_safety.temp_status[i].inverter_derating_on = 0;
		iib->iib_safety.temp_status[i].inverter_max_passed = 1;
		//inverter_derating = 0;	/** Return 0 torque */
	}


	/// Verify IGBT temperature
	if(iib->inverter[i].temp_IGBT > WARNING_IGBT_TEMP && iib->inverter[i].temp_IGBT < MAX_IGBT_TEMP){

		/// We entered derating
		iib->iib_safety.temp_status[i].IGBT_derating_on = 1;
		/*igbt_derating = derating_linear_form((-21.0/(MAX_IGBT_TEMP - WARNING_IGBT_TEMP)), 21,
			(iib->inverter[i].temp_IGBT - WARNING_IGBT_TEMP));*/
	}
	else if(iib->inverter[i].temp_inverter >= MAX_IGBT_TEMP){	/** We passed the max temperature */

		iib->iib_safety.temp_status[i].IGBT_derating_on = 0;
		iib->iib_safety.temp_status[i].IGBT_max_passed = 1;
		//igbt_derating = 0;	/** Return 0 torque */
	}


	/// Verify Motor temperature
	if(iib->inverter[i].temp_motor > WARNING_MOTOR_TEMP && iib->inverter[i].temp_motor < MAX_MOTOR_TEMP){

		/// We entered derating
		iib->iib_safety.temp_status[i].motor_derating_on = 1;
		/*motor_temp_derating = derating_linear_form((-21.0/(MAX_MOTOR_TEMP - WARNING_MOTOR_TEMP)), 21,
			(iib->inverter[i].temp_motor - WARNING_MOTOR_TEMP));*/
	}
	else if(iib->inverter[i].temp_inverter >= MAX_MOTOR_TEMP){	/** We passed the max temperature */

		iib->iib_safety.temp_status[i].motor_derating_on = 0;
		iib->iib_safety.temp_status[i].motor_max_passed = 1;
		//motor_temp_derating = 0;	/** Return 0 torque */
	}


	/// If every temperature is under the normal operation range
	/*if(motor_temp_derating == 21000 && igbt_derating == 21000 && inverter_derating == 21000){

		iib->iib_safety.temp_status[i].inverter_derating_on = 0;
		iib->iib_safety.temp_status[i].IGBT_derating_on 	= 0;
		iib->iib_safety.temp_status[i].motor_derating_on 	= 0;
		iib->iib_safety.temp_status[i].inverter_max_passed 	= 0;
		iib->iib_safety.temp_status[i].IGBT_max_passed 		= 0;
		iib->iib_safety.temp_status[i].motor_max_passed 	= 0;

		return 21000;	/// return the max torque available

	}*/
	/// At least one derating is on, check wich derating limits the most

	return 0;	/// if non of this conditions are meet, something is wrong with the code
}


/**
*	Name: motor_power_curve;
*	Description: Describes the motor curve power vs rpm
*	Input's: actual motor rpm, max values for torque and power
*	Output: corresponding torque we want to send to the motor in N/m *1000
*/
uint16_t motor_curve(int16_t rpm, uint16_t max_torque, uint16_t max_power){

	int16_t w = (rpm*2*PI) / 60;	/** Convert rpm to rad/s */
	uint16_t abs_rpm = (uint16_t) abs(rpm);	/** Makes every thing the same for negative torques */
	float derating_rpm = ((max_power*1000)/max_torque)*(60/(2*PI));	/** Computes the maximum rpm before derate */

	/// Not in derate zone
	if(abs_rpm <= derating_rpm){

		return max_torque;
	}

	/// In derate zone
	else if(abs_rpm >= derating_rpm && abs_rpm <= MAX_RPM){

		return (1000*((max_power)/w));	/** Torque = power/w , (w rad/s) */
	}

	return 0;
}


/**
*	Name: minimum;
*	Description: Returns lowest of 7 values
*	Input's: all values (a,b,c,d,e, f, g)
*	Output: minimum value
*/
uint16_t minimum(uint16_t a, uint16_t b, uint16_t c, uint16_t d, uint16_t e, uint16_t f, uint16_t g){

	uint16_t min = 0;

	min = MIN(a,b);
	min = MIN(min,c);
	min = MIN(min,d);
	min = MIN(min,e);
	min = MIN(min,f);
	min = MIN(min,g);

	return min;
}


/**
*   Name: calculate_total_power
*   Description: Calculate the sum of all motor's power
*   Input's: IIB structure
*   Output: void
*/
uint16_t calculate_total_power(IIB *iib){

    int i = 0;

    uint16_t power[i];

    for(i = 0; i < 4; i++){

        /// individual motor power Power = torque * speed converter rpm to (rad/s)
        power[i] = iib->inverter[i].torque/10000 * ((iib->inverter[i].actual_speed*2*PI)/60);
    }

    return power[0] + power[1] + power[2] + power[3];   /** total Tractive System power */
}


/**
*   Name: power_limitation
*   Description: If the max power was excedeed, reduce torque in each motor the same value
*   Input's: IIB structure, actual Tractive System power
*   Output: void
*/
/*void power_limitation(IIB *iib, uint16_t actual_power){

	int16_t power_difference = actual_power - MAX_CAR_POWER;
	int i = 0;
	float torque_reduction;
	uint16_t speed_sum = 0;

	/// Shit happened
	if(power_difference < 0){

		return;
	}

	/// Sum all speeds in rad/s
	for(i = 0; i < 4; ++i){

		speed_sum += (iib->inverter[i].actual_speed/60.0)*2*PI;
	}

	/// Calculate equal torque reduction to each motor
	torque_reduction = -1.0*(MAX_CAR_POWER - actual_power)/speed_sum;

	/// Update new torque
	for(i = 0; i < 4; i++){

		iib->inverter[i].setpoints.torque_limit_positive += torque_reduction;
	}
}*/


/**
*	Name: apply limits;
*	Description: Verifies if the wich of the torques is the minimum, safety, operation, derating_maximum, derating_operation or the current value, and returns it, also checks if speed isnt above maximum
*	Input's: all values (a,b,c,d,e)
*	Output: minimum value
*/
void apply_limits(IIB * iib){

	int msg_is_good = 0, setpoint_error_margin = 10;
	uint16_t derating_torque_operation = 0, derating_torque_safety = 0;	/** Maximum's from derating curves */
	uint16_t voltage_derating_torque = 0, temperature_derating = 0, total_power = 0;	/// Maximum's from other deratings

	/// Original values
	uint16_t original_torque[4];
	int16_t original_speed[4];

	int i = 0;

	/// Checks for the 4 inverters
	for(i = 0; i < 4; i++){

		/// Keep original values, for later comparison
		original_torque[i] = iib->inverter[i].setpoints.torque_limit_positive;
	 	original_speed[i]  = iib->inverter[i].setpoints.target_setpoint;

		/// Calculate all deratings
		voltage_derating_torque = voltage_deratings(iib);

	 	derating_torque_operation = motor_curve(iib->inverter[i].actual_speed,
	 		iib->iib_safety.inverter_limits[i].max_operation_torque,
	 		iib->iib_safety.inverter_limits[i].max_operation_power);

	 	derating_torque_safety = motor_curve(iib->inverter[i].actual_speed,
	 		iib->iib_safety.inverter_limits[i].max_safety_torque, iib->iib_safety.inverter_limits[i].max_safety_power);

	 	temperature_derating = temperature_deratings(i, iib);

		/// Returns the minimum value "safest value"
		iib->inverter[i].setpoints.torque_limit_positive = minimum(iib->iib_safety.inverter_limits[i].max_safety_torque,
			iib->iib_safety.inverter_limits[i].max_operation_torque, iib->inverter[i].setpoints.torque_limit_positive,
			derating_torque_operation, derating_torque_safety, 21000, temperature_derating);


		/// Checks speed limits
		if(iib->inverter[i].setpoints.target_setpoint >= MAX_RPM){

			iib->inverter[i].setpoints.target_setpoint = MAX_RPM;	/** Reduce to the maximum alowed */
		}

		/// If speed is negative
		// if(iib->inverter[i].setpoints.target_speed < 0){

		//  	/// If regeneration is active check "maximum"
		// 	if(iib->regen_on == 1){

		// 		if(iib->inverter[i].setpoints.target_speed <= -1*MAX_RPM){

		//  			iib->inverter[i].setpoints.target_speed = -1*MAX_RPM;	/** Reduce to the "maximum" alowed */

		// 		}else if(iib->regen_on == 0) {

		//  			/// no negative speeds alowed
		// 			iib->inverter[i].setpoints.target_speed = 0;
		// 		}
		// 	}
		// }

		/// Sees if original message is the same as the final, if all is the same we consider a good arm message
		/// Apply a margin to setpoints
		if(original_torque[i] <= iib->inverter[i].setpoints.torque_limit_positive + setpoint_error_margin &&
			original_speed[i] <= iib->inverter[i].setpoints.target_setpoint + setpoint_error_margin
			&& iib->car.status.arm_received_msg == 1 && (iib->input_mode != DEFAULT_MODE && iib->input_mode != ERROR_MODE)){

			msg_is_good++;
		}
	}

	/// Verify if the 4 setpoints are good, only if ARM is responsible by them or it is Ok
	if(msg_is_good == 4 && iib->car.status.arm_ok == 1
		&& (iib->input_mode != DEFAULT_MODE && iib->input_mode != ERROR_MODE)){

		good_arm_msg_timer = 0;
	}

	iib->car.status.arm_received_msg = 0;	/** Clear ARM received messages flag */


	/// Final check, if any of this conditions is set we set idle
	if(iib->turning_on == 1 || iib->turning_off == 1 || iib->inv_rtd == 0 || iib->car.status.car_rtd == 0
		|| iib->input_mode == ERROR_MODE){

		set_idle_all(iib);
	}

	/// Check if TE is ok, redundacy check, only do this if we are not in test bench
	if(iib->input_mode != TEST_BENCH_MODE){

		if(iib->car.status.te_ok == 0){

			set_idle_all(iib);
		}
	}

	return;
}


/**
*	Name: max_temperature_check;
*	Description: Verifies if no temperature is above maximum values
*	Input's: IIB structure
*	Output: binary value 1 or 0, 1 meaning not good, and 0 every thing ok
*/
int max_temperature_check(IIB *iib){

	int i = 0, max_passed = 0;

	/// Verifies the 4 inverters/motor
	for(i = 0; i < 4; i++){

		if(iib->iib_safety.temp_status[i].inverter_max_passed == 1){

			max_passed = 1;
		}
		if(iib->iib_safety.temp_status[i].IGBT_max_passed == 1){

			max_passed = 1;
		}
		if(iib->iib_safety.temp_status[i].motor_max_passed == 1){

			max_passed = 1;
		}
	}

	/// At least one temperature is above maximum
	if(max_passed == 1){

		return 1;
	}

	/// All temperatures are fine, should return 0
	return max_passed;
}

/*void inverter_wtd_state(IIB *iib){

	if(wtd_FL > 100 || wtd_FR > 100 || wtd_RL > 100 || wtd_RR > 100){

		if(iib->inv_on == 1){

			iib->inv_rtd = 0;
			turn_off_sequence(WE_TURN_OFF, iib);
		}
		iib->inv_rtd = 0;
	}


}*/


/**
*	Name: safety_check;
*	Description: Verifies car devices and iib condition, all safety states are checked, applys setpoint limits
*				 deratings, i2t, max limits and operation limits
*
*	Input's: IIB structure
*	Output: void
*/
void safety_check(IIB *iib){

	/// Verify car devices(ARM/TE) state, ignore this if we are in test bench mode
	if(iib->input_mode != TEST_BENCH_MODE){

		/// Check if ARM stoped sending messages (1 s)
		if(wtd_arm > 10 && iib->inv_on == 1){

	    	iib->car.status.arm_ok = 0;
	    	/// change to default mode, setpoints defined by TE
	    	iib->input_mode = DEFAULT_MODE;
	    }

	    /// Check if ARM is sending bad setpoints
	    if(good_arm_msg_timer > 15){

	    	iib->car.status.arm_stupid = 1;
	    	/// change to default mode, setpoints defined by TE
	    	iib->input_mode = DEFAULT_MODE;
	    }

	    /// Check if TE stoped sending messages (500 ms)
	    if(wtd_torque_encoder > 1){

	    	iib->car.status.te_ok = 0;
	    	iib->car.te_alive_again = 0;
	    	set_idle_all(iib);	/** We must put all motor's to idle(No pedal!!!) */

	    	/// Worst mode possible
	    	iib->input_mode = ERROR_MODE;
	    }
	}


	/// We are in test bench conditions
	if(iib->input_mode == TEST_BENCH_MODE){

		if(wtd_arm > 10){	/** (1 s) */

	    	iib->car.status.arm_ok = 0;		/// Just to signal that ARM is not sending messages
	    }
	}


	/// Verify IIB RTD conditions, High voltage in the inverters and all temperatures ok
	if(iib->car.status.inv_hv == 1 /*&& max_temperature_check(iib) == 0*/){

		iib->inv_rtd = 1;	/** Every thing ok */

	}else{

		iib->inv_rtd = 0;	/** inverters are not in RTD */
	}

	/// Aply the setpoint limits, final stage  before sending to amk inverters
	apply_limits(iib);

	return;
}

/**
*    Name: verify_TSAL
*    Description: Updates TSAL status
*    Input's: IIB structure
*    Output: void
*/

void verify_TSAL(IIB *iib){

     iib->car.status.tsal_status= TSAL_INV_READ;
}

void reset_amk_erros(IIB *iib){

	int i = 0;

	if(iib->allow_reset_errors){

		for(i = 0; i < 4; i++){

			if(iib->inverter[i].error_info != 0){
		 		set_reset(iib, i);
		 		iib->iib_safety.error_latch[i] = 1;
		 	}


		 	if(iib->iib_safety.error_latch[i] == 1){

		 		if(iib->inverter[i].error_info == 0){

		 			iib->iib_safety.error_latch[i] = 0;

		 			if(iib->inv_on){

		 				set_idle(iib, i);
		 				set_turn_on(iib, i);

		 			}else if(!iib->inv_on){

		 				set_turn_off(iib, i);
		 			}
		 		}
		 	}
		}

		// if(iib->inverter[i].error_info != 0){

		// 	set_reset(iib, i);
		// 	iib->iib_safety.error_latch[i] = 1;

		// 	if(i == 0 || i == 1){

		// 		if(iib->rear_axis_status){

		// 			set_idle(iib, iib->inverter[i].same_axis_inverter);
		// 		}

		// 	}else if(i == 2 || i == 3){

		// 		if(iib->front_axis_status){

		// 			set_idle(iib, iib->inverter[i].same_axis_inverter);
		// 		}
		// 	}
	}

	return;
}

void handle_amk_speed_control(IIB *iib){

	uint16_t power_derating_operation_pos[4], power_derating_safety_pos[4];
	uint16_t power_derating_operation_neg[4], power_derating_safety_neg[4];

	int i = 0;

	for(i = 0; i < 4; i++){

		/// Compute power curve with normal operation parameters
		power_derating_operation_pos[i] = motor_curve(iib->inverter[i].actual_speed,
			iib->iib_safety.inverter_limits[i].max_operation_torque,
			iib->iib_safety.inverter_limits[i].max_operation_power);

		/// Compute power curve with safety parameters
		power_derating_safety_pos[i] = motor_curve(iib->inverter[i].actual_speed,
	 		iib->iib_safety.inverter_limits[i].max_safety_torque,
	 		iib->iib_safety.inverter_limits[i].max_safety_power);

		/// Limit value to the lowest one
		iib->inverter[i].setpoints.torque_limit_positive = minimum(power_derating_operation_pos[i],
			power_derating_safety_pos[i], iib->iib_safety.inverter_limits[i].max_operation_torque,
			iib->iib_safety.inverter_limits[i].max_safety_torque, iib->inverter[i].setpoints.torque_limit_positive, 21000, 21000);

		/*
		 * Limit negative torque
		 */

		if(!iib->regen_on){

			iib->inverter[i].setpoints.torque_limit_negative = 0;

		}else if(iib->regen_on){

			/// Limits negative torque to only negative values
			if(iib->inverter[i].setpoints.torque_limit_negative > 0){
				iib->inverter[i].setpoints.torque_limit_negative = 0;
			}

			power_derating_operation_neg[i] = motor_curve(abs(iib->inverter[i].actual_speed),
				abs(iib->iib_safety.inverter_limits[i].max_operation_regen_torque), MAX_REGEN_POWER);

			power_derating_safety_neg[i] = motor_curve(abs(iib->inverter[i].actual_speed),
				abs(iib->iib_safety.inverter_limits[i].max_safety_regen_torque), MAX_REGEN_POWER);

			iib->inverter[i].setpoints.torque_limit_negative = -1*(minimum(abs(iib->inverter[i].setpoints.torque_limit_negative),
				abs(iib->iib_safety.inverter_limits[i].max_operation_regen_torque),
				abs(iib->iib_safety.inverter_limits[i].max_safety_regen_torque), power_derating_operation_neg[i],
				power_derating_safety_neg[i], 21000, 21000));
		}

		/// Limit speed
		if(iib->inverter[i].setpoints.target_setpoint > iib->iib_safety.max_rpm){

			iib->inverter[i].setpoints.target_setpoint = iib->iib_safety.max_rpm;
		}
	}

	return;
}


void ez_safety(IIB *iib){

	uint16_t ola;

	int i = 0;

	if(iib->inv_on){
		LATDbits.LATD2 = 1;
	}
	else{
		LATDbits.LATD2 = 0;
	}


	for(i = 0; i < 4; i++){

		if(iib->inverter[i].setpoints.torque_limit_positive > iib->iib_safety.inverter_limits[i].max_operation_torque){

			iib->inverter[i].setpoints.torque_limit_positive = iib->iib_safety.inverter_limits[i].max_operation_torque;
		}

		uint16_t curve_value = get_accel_curve(iib, i);
        if ( curve_value < iib->inverter[i].setpoints.torque_limit_positive ) {
		    iib->inverter[i].setpoints.torque_limit_positive = curve_value;
        }

		if(iib->regen_on){

			if(iib->inverter[i].setpoints.torque_limit_negative < iib->iib_safety.inverter_limits[i].max_operation_regen_torque){

				iib->inverter[i].setpoints.torque_limit_negative = iib->iib_safety.inverter_limits[i].max_operation_regen_torque;
			}

			if(iib->inverter[i].actual_speed <= KMH_5){

				iib->inverter[i].setpoints.torque_limit_negative = 0;

			}else if(iib->inverter[i].actual_speed > KMH_5 && iib->inverter[i].actual_speed < (KMH_5*4)){

				iib->inverter[i].setpoints.torque_limit_negative = (-1)*derating_linear_form((-1)*(abs(iib->inverter[i].setpoints.torque_limit_negative)/3000),
				 abs(iib->inverter[i].setpoints.torque_limit_negative), (KMH_5*4) - iib->inverter[i].actual_speed);
			}

		}else{

			iib->inverter[i].setpoints.torque_limit_negative = 0;
		}

		ola = temperature_deratings(i, iib);
	}

	if(iib->amk_control_mode == SPEED_CONTROL){

		for(i = 0; i < 4; i++){

			if(iib->inverter[i].setpoints.target_setpoint > iib->iib_safety.max_rpm){

				iib->inverter[i].setpoints.target_setpoint = iib->iib_safety.max_rpm;
			}

		}
	}else if(iib->amk_control_mode == TORQUE_CONTROL){

		for(i = 0; i < 4; i++){

			if(iib->inverter[i].setpoints.target_setpoint > iib->inverter[i].setpoints.torque_limit_positive){

				iib->inverter[i].setpoints.target_setpoint = iib->inverter[i].setpoints.torque_limit_positive;

			}else if(iib->inverter[i].setpoints.target_setpoint < 0 && iib->inverter[i].setpoints.target_setpoint >= iib->inverter[i].setpoints.torque_limit_negative){

				if(!iib->regen_on){

					iib->inverter[i].setpoints.target_setpoint = 0;
				}

			}else if(iib->inverter[i].setpoints.target_setpoint < iib->inverter[i].setpoints.torque_limit_negative){

				if(iib->regen_on){

					iib->inverter[i].setpoints.target_setpoint = iib->inverter[i].setpoints.torque_limit_negative;

				}else{

					iib->inverter[i].setpoints.target_setpoint = 0;
				}
			}
		}
	}


	if(iib->input_mode != TEST_BENCH_MODE){

		if(wtd_torque_encoder > 50){
			iib->car.status.te_ok = 0;
			set_idle_all(iib);
		}

		if(wtd_arm > 200 && iib->inv_on == 0){
			iib->car.status.arm_ok = 0;
			if(iib->input_mode == ARM_MODE){
				iib->input_mode = DEFAULT_MODE;

				iib->arm_wtd_exceded = 1;
				wtd_revive_arm = 0;
			}
		}

		/// Arm wtd timer is shorter in case the inverters are on
		if(wtd_arm > 50 && iib->inv_on == 1){
			iib->car.status.arm_ok = 0;
			if(iib->input_mode == ARM_MODE){
				iib->input_mode = DEFAULT_MODE;

				iib->arm_wtd_exceded = 1;
				wtd_revive_arm = 0;
			}
		}
	}

	if(iib->car.status.inv_hv){
		iib->inv_rtd = 1;
	}
	else if(!iib->car.status.inv_hv){
		iib->inv_rtd = 0;
	}

	//verify_shut_circuit(iib);

	if(iib->turning_on == 1 || iib->turning_off == 1 || iib->inv_rtd == 0 || iib->car.status.car_rtd == 0){

		set_idle_all(iib);
	}

  //verify system ready
	for(i=0 ; i<4 ; i++){
		if(!iib->inverter[i].status.system_ready){
			set_idle(iib,i);
		}
	}

	reset_amk_erros(iib);

	return;

}
